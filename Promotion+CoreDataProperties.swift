//
//  Promotion+CoreDataProperties.swift
//  Promotions
//
//  Created by DFINLAY-AIR on 25/04/16.
//  Copyright © 2016 dcf.matcom. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

import Foundation
import CoreData

extension Promotion {

    @NSManaged var promotion: String?
    @NSManaged var promotiondescription: String?
    @NSManaged var promotionproduct: String?

}
