//
//  FlickrSearchResults.swift
//  Promotions
//
//  Created by DFINLAY-AIR on 27/04/16.
//  Copyright © 2016 dcf.matcom. All rights reserved.
//

import Foundation
struct FlickrSearchResults {
    let searchTerm : String
    let searchResults : [FlickrPhoto]
}
