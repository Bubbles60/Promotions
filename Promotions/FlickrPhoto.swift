//
//  FlickPhoto.swift
//  Flickr Search
//
//  Created by Phil Eggel on 05/10/2015.
//  Copyright © 2015 PhilEagleDev.com. All rights reserved.
//

import Foundation
import UIKit

class FlickrPhoto: NSObject {
    
    var thumbnail: UIImage?
    var largeImage: UIImage?
    
    // Lookup info
    var photoID: String
    var farm: Int
    var server: String
    var secret: String
    
    init(farm: Int?, server: String?, secret: String?, photoID: String?) {
        self.farm = farm ?? 0
        self.server = server ?? ""
        self.secret = secret ?? ""
        self.photoID = photoID ?? ""
    }
}