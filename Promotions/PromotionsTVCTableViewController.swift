//
//  PromotionsTVCTableViewController.swift
//  Promotions
//
//  Created by DFINLAY-AIR on 25/04/16.
//  Copyright © 2016 dcf.matcom. All rights reserved.
//

import UIKit
import CoreData

class PromotionsTVCTableViewController: UITableViewController {
    
    var proms = [AnyObject]()
    // Retreive the managedObjectContext from AppDelegate once only
    let managedObjectContext = (UIApplication.sharedApplication().delegate as! AppDelegate).managedObjectContext

    override func viewDidLoad() {
        super.viewDidLoad()
        //

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
         self.navigationItem.rightBarButtonItem = self.editButtonItem()
    }
    
    override func viewWillAppear(animated: Bool) {
       loadPromotions()
        tableView.reloadData()
    }
    
    func loadPromotions()
    { 
        let request = NSFetchRequest(entityName: "Promotion")
        request.returnsObjectsAsFaults = true
 
        do {
            proms = try managedObjectContext.executeFetchRequest(request)
            
            for prom in proms {
                let thisPromo = prom as! Promotion
                print( thisPromo.promotion!)
                print(thisPromo.promotiondescription!)
            }
            
            //5
            //promotiondb.append(person)
        } catch let error as NSError  {
            print("Could not save \(error), \(error.userInfo)")
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    // MARK: - Table view data source
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return proms.count
    }
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("Cell", forIndexPath: indexPath)
        let myprom = proms[indexPath.row] as! Promotion
         // Configure the cell...
        cell.textLabel!.text = myprom.promotion
        cell.detailTextLabel!.text = myprom.promotiondescription
        return cell
    }
    /*
    // Override to support conditional editing of the table view.
    override func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

  
    // Override to support editing the table view.
    override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        if editingStyle == .Delete {
            // Delete the row from the data source
            let thisPromo = proms[indexPath.row] as! Promotion
            let thisprom = thisPromo.promotion!
             //
            proms.removeAtIndex(indexPath.row)
            tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
            // Delete item from data base
            deleteFromDB(thisprom)
         } else if editingStyle == .Insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    func deleteFromDB(dbitem: String) {
        
         //2
        let request = NSFetchRequest(entityName: "Promotion")
        request.returnsObjectsAsFaults = true
        request.predicate = NSPredicate(format: "promotion = %@", dbitem)
        //
        do {
            let promsaa = try managedObjectContext.executeFetchRequest(request)
            
            for prom in promsaa {
                let thisPromo = prom as! Promotion
                managedObjectContext.deleteObject(thisPromo)
            }
            try managedObjectContext.save()
            
            //5
            //promotiondb.append(person)
        } catch let error as NSError  {
            print("Could not Delete \(error), \(error.userInfo)")
        }
        
    }

    /*
    // Override to support rearranging the table view.
    override func tableView(tableView: UITableView, moveRowAtIndexPath fromIndexPath: NSIndexPath, toIndexPath: NSIndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(tableView: UITableView, canMoveRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        if segue.identifier == "updatePromotion" {
            if let indexPath = self.tableView.indexPathForSelectedRow {
                let thisPromo = proms[indexPath.row] as! Promotion
                let thisprom = thisPromo.promotion!
                print("Selected Promo \(thisprom)")
                
                let controller = segue.destinationViewController as! PromotionDetailViewController
                print("Selected Promo  2 \(thisprom)")
                controller.selectedPromotion = thisPromo
                
                // Will not be initialized
                //controller.promotion?.text = "Text"
                
                
                
                /*
                
                controller.detailItem = object
                controller.navigationItem.leftBarButtonItem = self.splitViewController?.displayModeButtonItem()
                controller.navigationItem.leftItemsSupplementBackButton = true*/
            }
        }
        
    }
    

}
