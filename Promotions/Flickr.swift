//
//  Flickr.swift
//  Flickr Search
//
//  Created by Phil Eggel on 05/10/2015.
//  Copyright © 2015 PhilEagleDev.com. All rights reserved.
//

import Foundation
import UIKit

typealias FlickrSearchCompletionBlock = (searchTerm: String, results: [FlickrPhoto]?, errorText: String?) -> ()
typealias FlickrPhotoCompletionBlock = (photoImage: UIImage?, errorText: String?) -> ()

class Flickr {

    private static let kFlickAPIKey = "978d7216fb7560fbd505ddc25c7bc264"
    
    private static func flickrSearchURLForSearchTerm(var searchTerm: String) -> String {
        searchTerm = searchTerm.stringByAddingPercentEncodingWithAllowedCharacters(NSCharacterSet.URLQueryAllowedCharacterSet())!
        return "https://api.flickr.com/services/rest/?method=flickr.photos.search&api_key=\(kFlickAPIKey)&text=\(searchTerm)&per_page=20&format=json&nojsoncallback=1"
    }
    
    static func flickrPhotoURLForFlickrPhoto(flickrPhoto: FlickrPhoto, size: String = "m") -> String {
        return "https://farm\(flickrPhoto.farm).staticflickr.com/\(flickrPhoto.server)/\(flickrPhoto.photoID)_\(flickrPhoto.secret)_\(size).jpg"
    }
    
    func searchFlickrForTerm(term: String, completion completionBlock: FlickrSearchCompletionBlock) {
        var searchURL = Flickr.flickrSearchURLForSearchTerm(term)
        
        let queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0)
        dispatch_async(queue) { () -> Void in
            
            // chargement du résultat (indication de l'erreur en cas de problème)
            guard let searchResultString = try? String(contentsOfURL: NSURL(string: searchURL)!, encoding: NSUTF8StringEncoding) else {
                completionBlock(searchTerm: term, results: nil, errorText: "unable to load the url \(searchURL)")
                return
            }
            
            let jsonData = searchResultString.dataUsingEncoding(NSUTF8StringEncoding)!
            guard let searchResults = try? NSJSONSerialization.JSONObjectWithData(jsonData, options: NSJSONReadingOptions(rawValue: 0)) else {
                completionBlock(searchTerm: term, results: nil, errorText: "unable to parse the result \(searchResultString)")
                return
            }
            
            guard let searchResultsDict = searchResults as? [String: AnyObject] else {
                completionBlock(searchTerm: term, results: nil, errorText: "Dictionary format not compatible")
                return
            }
            
            let status = searchResultsDict["stat"] as? String
            if status == "fail" {
                let message = searchResultsDict["message"] as? String ?? "no message"
                completionBlock(searchTerm: term, results: nil, errorText: "error from Flickr Service: \(message)")
                return
            }
            
            let photos = searchResultsDict["photos"] as! [String: AnyObject]
            let objPhotos = photos["photo"] as! [[String: AnyObject]]
            
            var flickrPhotos: [FlickrPhoto] = []
            for objPhoto in objPhotos {
                
                let farm = objPhoto["farm"] as? Int                         // json int
                let server = objPhoto["server"] as? String                  // json string
                let secret = objPhoto["secret"] as? String                  // json string
                let photoID = objPhoto["id"] as? String                     // json string

                let photo = FlickrPhoto(farm: farm, server: server, secret: secret, photoID: photoID)
                searchURL = Flickr.flickrPhotoURLForFlickrPhoto(photo)
                print(searchURL)
                
                // erreur quelque part ici TODO
                let url = NSURL(string: searchURL)
                if let imageData = NSData(contentsOfURL: url!) {
                    photo.thumbnail = UIImage(data: imageData)
                }
                
                flickrPhotos.append(photo)
            }
            
            completionBlock(searchTerm: term, results: flickrPhotos, errorText: nil)
        }
    }
    
    static func loadImageForPhoto(photo: FlickrPhoto, thumbnail: Bool, completion completionBlock: FlickrPhotoCompletionBlock) {
        
        let size = thumbnail ? "m" : "b"
        let searchURL = Flickr.flickrPhotoURLForFlickrPhoto(photo, size: size)
        
        let queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0)
        dispatch_async(queue) { () -> Void in
            
            guard let imageData = try? NSData(contentsOfURL: NSURL(string: searchURL)!, options: NSDataReadingOptions(rawValue: 0)) else {
                completionBlock(photoImage: nil, errorText: "Failed to load image")
                return
            }
            
            let image = UIImage(data: imageData)
            if size == "m" {
                photo.thumbnail = image
            } else {
                photo.largeImage = image
            }
            
            completionBlock(photoImage: image, errorText: nil)
        }
    }
}