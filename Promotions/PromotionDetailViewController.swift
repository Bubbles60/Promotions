//
//  PromotionDetailViewController.swift
//  Promotions
//
//  Created by DFINLAY-AIR on 25/04/16.
//  Copyright © 2016 dcf.matcom. All rights reserved.
//

import UIKit
import CoreData

class PromotionDetailViewController: UIViewController {
    
    // Retreive the managedObjectContext from AppDelegate once only
    let managedObjectContext = (UIApplication.sharedApplication().delegate as! AppDelegate).managedObjectContext
    
    var selectedPromotion: Promotion?
    
    
    @IBOutlet weak var promotion: UITextField!
    @IBOutlet weak var promotinDescription: UITextField!
    @IBOutlet weak var promotionProduct: UITextField!
    
   

    override func viewDidLoad() {
        super.viewDidLoad()
        
        if let updProm = selectedPromotion {
            promotion.text = updProm.promotion
        }
        

        // Do any additional setup after loading the view.
    }

    // MARK: - Data Base
    @IBAction func updateDataBase(sender: AnyObject) {
        //2
        let entity =  NSEntityDescription.entityForName("Promotion",
            inManagedObjectContext:managedObjectContext)
        
        let promotiondb = Promotion(entity: entity!,
            insertIntoManagedObjectContext: managedObjectContext)
        //
        promotiondb.promotion = promotion.text
        promotiondb.promotiondescription = promotinDescription.text
        promotiondb.promotionproduct = promotionProduct.text
        //4
        do {
            try managedObjectContext.save()
            //5
            //promotiondb.append(person)
        } catch let error as NSError  {
            print("Could not save \(error), \(error.userInfo)")
        }
        print(promotiondb)
        print("Object Saved")
        navigationController?.popViewControllerAnimated(true)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
