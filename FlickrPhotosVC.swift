//
//  FlickrPhotosVC.swift
//  Promotions
//
//  Created by DFINLAY-AIR on 27/04/16.
//  Copyright © 2016 dcf.matcom. All rights reserved.
//

import UIKit



class FlickrPhotosVC: UICollectionViewController {
    var mysearches: [FlickrSearchResults] = []
    
    /** liste des mots clés recherchés */
    private var searches = OrderedDictionary<String, [FlickrPhoto]>()
    var myphotos: [FlickrPhoto] = []
   
 
    
    
    private let reuseIdentifier = "FlickrCell"
    private let sectionInsets = UIEdgeInsets(top: 50.0, left: 20.0, bottom: 50.0, right: 20.0)
    
   //private var searches = [FlickrSearchResults]()
    private let flickr = Flickr()
    /*
    func photoForIndexPath (indexPath: NSIndexPath) -> FlickrPhoto {
        return searches[indexPath.section].searchResults[indexPath.row]
        
    }*/

    override func viewDidLoad() {
        super.viewDidLoad()

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Register cell classes
        //self.collectionView!.registerClass(UICollectionViewCell.self, forCellWithReuseIdentifier: reuseIdentifier)

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using [segue destinationViewController].
        // Pass the selected object to the new view controller.
    }
    */

    // MARK: UICollectionViewDataSource
    //1
    override func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return mysearches.count
    }
    
    //2
    override func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        let currentlist = mysearches[section].searchResults.count
          return mysearches[section].searchResults.count
    }
    
    //3
    override func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
   
        //1
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier(reuseIdentifier, forIndexPath: indexPath) as! FlickrPhotoCell
        
        //2
        let flickrPhoto = mysearches[indexPath.section].searchResults[indexPath.row]
        //let flickrPhoto = mysearches[indexPath.section]
        cell.backgroundColor = UIColor.whiteColor()
        //3
        cell.photoCell.image = flickrPhoto.thumbnail
         return cell
    }

    // MARK: UICollectionViewDelegate
    
    override func collectionView(collectionView: UICollectionView,
        viewForSupplementaryElementOfKind kind: String,
        atIndexPath indexPath: NSIndexPath) -> UICollectionReusableView {
            //1
            switch kind {
                //2
            case UICollectionElementKindSectionHeader:
                //3
                let headerView =
                collectionView.dequeueReusableSupplementaryViewOfKind(kind,
                    withReuseIdentifier: "FlickrPhotoHeaderView",
                    forIndexPath: indexPath)
                    as! FlickrPhotoHeaderView
                headerView.labelHeader.text = mysearches[indexPath.section].searchTerm
                return headerView
            default:
                //4
                assert(false, "Unexpected element kind")
            }
    }

    /*
    // Uncomment this method to specify if the specified item should be highlighted during tracking
    override func collectionView(collectionView: UICollectionView, shouldHighlightItemAtIndexPath indexPath: NSIndexPath) -> Bool {
        return true
    }
    */

    /*
    // Uncomment this method to specify if the specified item should be selected
    override func collectionView(collectionView: UICollectionView, shouldSelectItemAtIndexPath indexPath: NSIndexPath) -> Bool {
        return true
    }
    */

    /*
    // Uncomment these methods to specify if an action menu should be displayed for the specified item, and react to actions performed on the item
    override func collectionView(collectionView: UICollectionView, shouldShowMenuForItemAtIndexPath indexPath: NSIndexPath) -> Bool {
        return false
    }

    override func collectionView(collectionView: UICollectionView, canPerformAction action: Selector, forItemAtIndexPath indexPath: NSIndexPath, withSender sender: AnyObject?) -> Bool {
        return false
    }

    override func collectionView(collectionView: UICollectionView, performAction action: Selector, forItemAtIndexPath indexPath: NSIndexPath, withSender sender: AnyObject?) {
    
    }
    */

}
    // MARK: - TextField Delegate
    extension FlickrPhotosVC: UITextFieldDelegate {
        // Note we are ina delegate because we want to search when user presses enter in textfield
        func textFieldShouldReturn(textField: UITextField) -> Bool {
            
            flickr.searchFlickrForTerm(textField.text ?? "") { (searchTerm, results, errorText) -> () in
                
                if results != nil && results?.count > 0 {
                  print("OK")
                    
                    // Save search results
                    if !self.searches.array.contains(searchTerm) {
                        print("Found \(results!.count) photo(s) matching \(searchTerm)")
                        self.searches[searchTerm] = results
                        self.myphotos = results!
                        //
                        let result1: FlickrSearchResults = FlickrSearchResults(searchTerm: searchTerm, searchResults: results! )
                        self.mysearches.append(result1)
                        
                        let dic = self.searches.dictionary
                        if dic.count > 0                        {
                            let firstKey = Array(dic.keys)[0]
                            print(dic[firstKey])
                            let photos = dic[firstKey]?.count
                        }
                    self.collectionView?.reloadData()
                    }
                }
                else {
                    print("Error searching Flickr: \(errorText)")
                }
            }
            
            textField.resignFirstResponder()
            return true
        }
}
extension FlickrPhotosVC : UICollectionViewDelegateFlowLayout {
    //1
    func collectionView(collectionView: UICollectionView,
        layout collectionViewLayout: UICollectionViewLayout,
        sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
            
            let flickrPhoto =  myphotos[indexPath.row]
            //2
            if var size = flickrPhoto.thumbnail?.size {
                size.width += 10
                size.height += 10
                return size
            }
            return CGSize(width: 100, height: 100)
    }
    
    //3
    func collectionView(collectionView: UICollectionView,
        layout collectionViewLayout: UICollectionViewLayout,
        insetForSectionAtIndex section: Int) -> UIEdgeInsets {
            return sectionInsets
    }
}
